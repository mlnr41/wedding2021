// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDcKvh5Zy4hKZi_yM2i4gESL4saWB4OnYg",
    authDomain: "wedding-21.firebaseapp.com",
    databaseURL:
      "https://wedding-21-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "wedding-21",
    storageBucket: "wedding-21.appspot.com",
    messagingSenderId: "674827593339",
    appId: "1:674827593339:web:1cc4f4709dffec63e2e4de",
    measurementId: "G-F1KB49V4WQ",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
