import { Component } from '@angular/core';
import {
  options,
  fullpage_api,
} from 'fullpage.js/dist/fullpage.extensions.min';
import { PageService } from './shared/page.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  config: options;
  fullpageApi: fullpage_api;

  constructor(public pageService: PageService) {
    const pageServiceLocal = this.pageService;
    this.config = {
      anchors: [
        'udvozlet',
        'info',
        'rsvp',
        'helyszin',
        'megkozelithetoseg',
        'ajandek',
        'vacsora-menu',
        'galeria',
      ],
      menu: '#menu',
      scrollingSpeed: '1000',
      navigation: false,
      navigationPosition: 'left',
      dragAndMove: 'fingersonly',
      navigationTooltips: [
        'Esküvő',
        'Üdvözlünk!',
        'rsvp',
        'Helyszín',
        'Megközelíthetőség',
        'Nászajándék',
        'Vacsora Menü',
        'Galéria',
      ],
      showActiveTooltip: true,
      verticalCentered: false,

      onLeave(origin, destination): void {
        if (destination.anchor === 'udvozlet') {
          pageServiceLocal.pageChanged('udvozlet');
        }

        if (destination.anchor === 'rsvp') {
          pageServiceLocal.pageChanged('rsvp');
        }

        if (destination.anchor === 'info') {
          pageServiceLocal.pageChanged('info');
        }

        if (destination.anchor === 'helyszin') {
          pageServiceLocal.pageChanged('helyszin');
        }

        if (destination.anchor === 'megkozelithetoseg') {
          pageServiceLocal.pageChanged('megkozelithetoseg');
        }

        if (destination.anchor === 'ajandek') {
          pageServiceLocal.pageChanged('ajandek');
        }

        if (destination.anchor === 'vacsora-menu') {
          pageServiceLocal.pageChanged('vacsora-menu');
        }

        if (destination.anchor === 'galeria') {
          pageServiceLocal.pageChanged('galeria');
        }
      },
    };
  }

  getRef(fullPageRef: any): void {
    this.fullpageApi = fullPageRef;
    this.pageService.setFullpageApi(fullPageRef);
  }
}
