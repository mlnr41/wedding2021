import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PageService } from 'src/app/shared/page.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
})
export class WelcomeComponent implements OnInit, OnDestroy {
  pageChangedSub: Subscription;
  // countdown: string;
  // weddingDate = new Date(2021, 8, 17, 18).getTime();

  constructor(private pageService: PageService) {}

  ngOnInit(): void {
    this.pageChangedSub = this.pageService.notifyObservable$.subscribe(
      (page) => {
        const device = this.pageService.checkDevice();
        if (page === 'udvozlet') {
          document.body.style.backgroundSize = 'cover';
        } else {
          switch (device) {
            case 'full':
              document.body.style.backgroundSize = '335%';
              break;
            case 'tablet':
              document.body.style.backgroundSize = '300%';
              break;
            case 'mobile':
              document.body.style.backgroundSize = '650%';
              break;
            case 'mobile S':
              document.body.style.backgroundSize = '650%';
              break;
          }
        }
      }
    );

    // let x = setInterval(() => {
    //   const now = new Date().getTime();
    //   const distance = this.weddingDate - now;

    //   const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    //   const hours = Math.floor(
    //     (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    //   );

    //   this.countdown = days + ' nap és ' + hours + ' óra ';
    // }, 1000);
  }

  ngOnDestroy(): void {
    this.pageChangedSub.unsubscribe();
  }
}
