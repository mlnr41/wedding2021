import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
	selector: 'app-rsvp',
	templateUrl: './rsvp.component.html',
	styleUrls: [ './rsvp.component.scss' ]
})
export class RsvpComponent implements OnInit {
	@ViewChild('rsvpForm', { static: true })
	rsvpForm: NgForm;

	@ViewChild('mainContainer', { static: true })
	mainContainer: ElementRef;

	@ViewChild('submitMessageYes', { static: true })
	submitMessageYes: ElementRef;

	@ViewChild('submitMessageNo', { static: true })
	submitMessageNo: ElementRef;

	canAttend;
	choiceMade = false;
	numberOfGuests: number;
	guests = [ 0 ];
	submitted;
	names = '';

	constructor(private firestore: AngularFirestore) {}

	ngOnInit() {
		this.rsvpForm.form.valueChanges.subscribe((value) => {
			if (value.canAttend === 'yes') {
				this.canAttend = 1;
				this.choiceMade = true;
			} else if (value.canAttend === 'no') {
				this.canAttend = -1;
				this.guests = [ 0 ];
				this.choiceMade = true;
			}

			if (value.numberOfGuests) {
				const number = parseInt(value.numberOfGuests);
				this.guests.length = number;
			}
		});
	}

	onSubmit(form) {
		const values = form.value;
		this.submitted = {
			canAttend: this.canAttend
		};
		const guestNames = [];
		if (values.canAttend === 'no') {
			guestNames.push(values.cannotAttendName);
		} else {
			for (let i = 0; i < 4; i++) {
				const name = values['name ' + i];
				if (name) guestNames.push(name);
			}
		}

		guestNames.forEach((name) => {
			guestNames[guestNames.indexOf(name)] =
				guestNames[guestNames.indexOf(name)].charAt(0).toUpperCase() +
				guestNames[guestNames.indexOf(name)].substring(1);
		});

		if (guestNames.length > 1) {
			const lastName = guestNames[guestNames.length - 1];
			guestNames.pop();
			this.names = guestNames.join(', ');
			this.names = this.names + ' és ' + lastName;
		} else {
			this.names = guestNames[0];
		}
		this.mainContainer.nativeElement.classList.add('submitted');
		if (values.canAttend === 'yes') {
			this.submitMessageYes.nativeElement.classList.add('active');
		} else if (values.canAttend === 'no') {
			this.submitMessageNo.nativeElement.classList.add('active');
		}
		this.firestore
			.collection('rsvps')
			.add(values)
			.then((response) => console.log(response))
			.catch((e) => console.log(e));
	}
}
