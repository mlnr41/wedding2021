import { Component, OnInit, ViewChild } from '@angular/core';
import Splide from '@splidejs/splide';
@Component({
  selector: 'app-dinner-menu',
  templateUrl: './dinner-menu.component.html',
  styleUrls: ['./dinner-menu.component.scss'],
})
export class DinnerMenuComponent implements OnInit {
  ngOnInit(): void {
    new Splide('#splide', {
      type: 'loop',
      perPage: 1,
      gap: 0,
      padding: 0,
      rewind: true,
      autoWidth: true,
      autoHeight: true,
      drag: false,
      classes: {
        arrows: 'splide__arrows dinner-menu__slide__arrows',
      }
    }).mount();
  }
}
