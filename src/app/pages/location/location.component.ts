import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { PageService } from 'src/app/shared/page.service';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss'],
})
export class LocationComponent implements OnInit, OnDestroy {
  pageChangeSub: Subscription;
  locationImages = [];
  @ViewChild('slideshow') slideshow: ElementRef;

  constructor(public pageService: PageService) {}

  ngOnInit(): void {
    for (let i = 1; i < 13; i++) {
      const imageSrc = '../../assets/images/Location/' + i + '.JPG';
      const image = { src: imageSrc, opacity: 0 };
      this.locationImages.push(image);
    }

    this.pageChangeSub = this.pageService.notifyObservable$.subscribe(
      (page) => {
        if (page === 'helyszin') {
          this.revealImages();
        }
      }
    );
  }

  revealImages(): void {
    setTimeout(() => {
      const shuffled = [...this.locationImages];
      const imageRevealInterval = setInterval(() => {
        shuffled.sort(() => 0.5 - Math.random());
        const image = shuffled[0];
        this.locationImages[this.locationImages.indexOf(image)].opacity = 0.7;
        shuffled.shift();
        if (shuffled.length === 0) {
          clearInterval(imageRevealInterval);
          this.slideshow.nativeElement.classList.add('loaded');
        }
      }, 200);
    }, 500);
  }

  ngOnDestroy(): void {
    this.pageChangeSub.unsubscribe();
  }
}
