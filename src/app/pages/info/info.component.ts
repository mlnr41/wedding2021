import { Container } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnDestroy, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { PageService } from 'src/app/shared/page.service';

declare var anime: any;

@Component({
	selector: 'app-info',
	templateUrl: './info.component.html',
	styleUrls: [ './info.component.scss' ]
})
export class InfoComponent implements OnInit, OnDestroy {
	pageChangedSub: Subscription;
	@ViewChild('container', { static: true })
	container: ElementRef;

	constructor(private pageService: PageService) {}

	ngOnInit(): void {
		this.pageChangedSub = this.pageService.notifyObservable$.subscribe((page) => {
			if (page === 'info') {
				setTimeout(() => {
					this.container.nativeElement.classList.add('loaded');
				}, 1500);
				const firstTextWrapper = document.querySelector('.an-1');
				firstTextWrapper.innerHTML = firstTextWrapper.textContent.replace(
					/\S/g,
					"<span class='letter'>$&</span>"
				);

				const secondTextWrapper = document.querySelector('.an-2');
				secondTextWrapper.innerHTML = secondTextWrapper.textContent.replace(
					/\S/g,
					"<span class='letter' style='opacity: 0'>$&</span>"
				);

				let tl = anime.timeline({
					easing: 'easeOutExpo',
					loop: false,
					duration: 900
				});

				tl.add({
					targets: '.an-1 .letter',
					scale: [ 4, 1 ],
					opacity: [ 0, 1 ],
					translateZ: 0,
					delay: (el, i) => 70 * i
				});

				tl.add({
					targets: '.an-2 .letter',
					scale: [ 4, 1 ],
					opacity: [ 0, 1 ],
					translateZ: 0,
					delay: (el, i) => 70 * i
				});
			}
		});
	}

	ngOnDestroy(): void {
		this.pageChangedSub.unsubscribe();
	}
}
