import { Component, Input, OnInit } from '@angular/core';
import { Loader } from "@googlemaps/js-api-loader";

@Component({
  selector: 'app-approachability',
  templateUrl: './approachability.component.html',
  styleUrls: ['./approachability.component.scss'],
})
export class ApproachabilityComponent implements OnInit {
  @Input() fullpageApi;
  map: google.maps.Map;
  loader = new Loader({
    apiKey: "AIzaSyDBVLQx0Sn0sOGzZfh7xnLC9Vjq4RTrjao",
    version: "weekly",
  });

  constructor() {}

  ngOnInit(): void {
    this.loader.load().then(() => {
      const csonakhaz = { lat: 47.432215054016595, lng: 19.091037875202485 };
      this.map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
        center: csonakhaz,
        zoom: 16,
        mapTypeId: "roadmap",
        mapId: "11f7628d8c2d2f57",
        gestureHandling: 'cooperative',
      } as google.maps.MapOptions);
      const marker = new google.maps.Marker({
        position: csonakhaz,
        map: this.map,
        animation: google.maps.Animation.DROP,
      })
    });
  }

}
