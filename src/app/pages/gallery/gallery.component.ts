import { Component, OnInit } from '@angular/core';
import { ImgServiceService } from './img-service.service';

export interface image {
  number: number;
  url: string;
  height: number;
  width: number;
}

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent implements OnInit {
  imagesToDisplay: image[];
  everyImage: image[];
  maxImages = 6;
  currentPage = 1;
  numberOfPages: number;

  imageClicked = false;
  currentPhotoSrc: string;

  constructor(private imgService: ImgServiceService) {}

  ngOnInit(): void {
    // this.imgService.getPhotoSources();

    // getImages().then((images) => {
    //   console.log(images);
    //   setTimeout(() => {
    //     images.sort((a, b) => {
    //       return a.number - b.number;
    //     });
    //     this.everyImage = images;
    //     this.imagesToDisplay = images.slice(0, this.maxImages);
    //     this.numberOfPages = Math.ceil(this.everyImage.length / this.maxImages);

    //   }, 500);
    // });
  }
}
