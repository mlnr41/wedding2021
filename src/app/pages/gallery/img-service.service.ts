import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { image } from './gallery.component';

@Injectable({
  providedIn: 'root',
})
export class ImgServiceService {
  constructor(public afStorage: AngularFireStorage) {} // private httpClient: HttpClient,

  // storageRef = this.afStorage.ref('Images');

  // getPhotoSources(): any[] {
  //   const images = [];
  //   this.storageRef.listAll().subscribe((list) => {
  //     list.items.forEach((item) => {
  //       const name = item.name;
  //       const id = +name.slice(0, -4);
  //       item.getDownloadURL().then((url) => {
  //         let img = new Image();
  //         img.src = url;
  //         let width = img.naturalWidth;
  //         let height = img.naturalHeight;
  //         images.push({ number: id, url, height, width });
  //       });
  //     });
  //   });
  //   return images;
  // }
}
