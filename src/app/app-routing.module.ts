import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApproachabilityComponent } from "./pages/approachability/approachability.component";
import { GalleryComponent } from "./pages/gallery/gallery.component";
import { LocationComponent } from "./pages/location/location.component";
import { WelcomeComponent } from './pages/welcome/welcome.component';

const routes: Routes = [
  // { path: 'udvozlo', component: WelcomeComponent },
  // { path: 'helyszin', component: LocationComponent},
  // { path: 'megkozelithetoseg', component: ApproachabilityComponent},
  // { path: 'galeria', component: GalleryComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
