import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class PageService {
  public api: any;
  private notify = new Subject<any>();
  /**
   * Observable string streams
   */
  notifyObservable$ = this.notify.asObservable();

  constructor() {}

  public pageChanged(page: string) {
    if (page) {
      this.notify.next(page);
    }
  }

  setFullpageApi(ref) {
    this.api = ref;
  }

  checkDevice() {
    let screenSize: string;
    if (window.matchMedia('(max-width: 320px)').matches) {
      screenSize = 'mobile S';
    } else if (window.matchMedia('(max-width: 420px)').matches) {
      screenSize = 'mobile';
    } else if (window.matchMedia('(max-width: 768px)').matches) {
      screenSize = 'tablet';
    } else {
      screenSize = 'full';
    }

    return screenSize;
  }
}
