import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { PageService } from '../shared/page.service';
@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss'],
})
export class NavigationBarComponent implements OnInit {
  pageChangedSub: Subscription;
  @ViewChild('sidebar') sidebar: ElementRef;
  @ViewChild('mobileMenu') mobileMenu: ElementRef;
  @ViewChild('burger') burger: ElementRef;
  navElements = [];

  constructor(private pageService: PageService, private renderer: Renderer2) {}

  ngOnInit(): void {
    const navItems = document.querySelectorAll('a');
    navItems.forEach((item) => {
      this.navElements.push(item);
    });
  }

  ngAfterViewInit() {
    this.pageChangedSub = this.pageService.notifyObservable$.subscribe(
      (page) => {
        if (page !== 'udvozlet') {
          this.renderer.setStyle(
            this.sidebar.nativeElement,
            'transform',
            'translateX(0)'
          );
        } else {
          this.renderer.setStyle(
            this.sidebar.nativeElement,
            'transform',
            'translateX(-50rem)'
          );
        }
        this.navElements.forEach((element) => {
          element.classList.remove('active');
          const navLink = element.href.split('#')[1];
          if (navLink === page) {
            element.classList.add('active');
          }
        });
      }
    );
  }

  goToHomePage() {
    this.pageService.api.moveTo(1);
  }

  toggleMenu(event) {
    this.burger.nativeElement.classList.toggle('open');
    this.mobileMenu.nativeElement.classList.toggle('open');
  }
}
