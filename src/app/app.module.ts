import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { LocationComponent } from './pages/location/location.component';
import { ApproachabilityComponent } from './pages/approachability/approachability.component';
import { GalleryComponent } from './pages/gallery/gallery.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { ImgServiceService } from './pages/gallery/img-service.service';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { AngularFullpageModule } from '@fullpage/angular-fullpage';
import { InfoComponent } from './pages/info/info.component';
import { DinnerMenuComponent } from './pages/dinner-menu/dinner-menu.component';
import { CrystalLightboxModule } from '@crystalui/angular-lightbox';
import { PageService } from './shared/page.service';
import { RsvpComponent } from './pages/rsvp/rsvp.component';
import { FormsModule } from '@angular/forms';
import { PresentsComponent } from './pages/presents/presents.component';

@NgModule({
	declarations: [
		AppComponent,
		WelcomeComponent,
		LocationComponent,
		ApproachabilityComponent,
		GalleryComponent,
		NavigationBarComponent,
		InfoComponent,
		DinnerMenuComponent,
		RsvpComponent,
		PresentsComponent
	],
	imports: [
		CrystalLightboxModule,
		CommonModule,
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		LazyLoadImageModule,
		AngularFireModule.initializeApp(environment.firebaseConfig),
		AngularFireStorageModule,
		AngularFireDatabaseModule,
		AngularFullpageModule,
		FormsModule
	],
	providers: [ ImgServiceService, PageService ],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
